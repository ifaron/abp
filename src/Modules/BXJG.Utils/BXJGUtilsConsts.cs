﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Utils
{
    public class BXJGUtilsConsts
    {
        public const string LocalizationSourceName = "BXJGUtils";

        #region 通用树的菜单和权限
        public const string GeneralTreeMenuName = "GeneralTreeMenu";
        //public const string GeneralTreeGetPermissionName = "GeneralTreeGetPermission";
        public const string GeneralTreeCreatePermissionName = "GeneralTreeCreatePermission";
        public const string GeneralTreeUpdatePermissionName = "GeneralTreeUpdatePermission";
        public const string GeneralTreeDeletePermissionName = "GeneralTreeDeletePermission";
        #endregion
    }
}
