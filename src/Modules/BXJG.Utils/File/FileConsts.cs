﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.Utils.File
{
    public class FileConsts
    {
        #region EntityFileMaxLength
        public const int EntityFileEntityTypeMaxLength = 100;
        public const int EntityFileEntityIdMaxLength = 60;
        public const int EntityFileFileUrlMaxLength = 500;
        public const int EntityFilePropertyNameMaxLength = 100;
        #endregion
    }
}
