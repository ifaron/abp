﻿using BXJG.GeneralTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.GoodsInfo.Application.Admin
{
    /// <summary>
    /// 后台管理物品分类的编辑模型
    /// </summary>
    public class GoodsInfoCategoryEditDto: GeneralTreeNodeEditBaseDto
    {
    }
}
