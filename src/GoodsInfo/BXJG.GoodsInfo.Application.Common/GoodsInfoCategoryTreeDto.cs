﻿using BXJG.GeneralTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.GoodsInfo.Application.Common
{
    /// <summary>
    /// 获取物品分类树形下拉框模型
    /// </summary>
    public class GoodsInfoCategoryTreeDto : GeneralTreeNodeDto<GoodsInfoCategoryTreeDto>
    {
    }
}
