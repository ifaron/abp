﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.CMS
{
    /// <summary>
    /// 内容类型
    /// </summary>
    public enum  ContentType
    {
        /// <summary>
        /// 文章
        /// </summary>
        Article
    }
}
