﻿using BXJG.Common;
using BXJG.GeneralTree;
using BXJG.Utils.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZLJ.BaseInfo.Administrative
{
    public class AdministrativeCombboxDto : GeneralTreeComboboxDto
    {
        public AdministrativeLevel Level { get; set; }
    }
}
