﻿using Abp.Application.Services.Dto;
using BXJG.Utils.File;
using BXJG.WorkOrder.WorkOrder;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.EmployeeApplication.WorkOrder
{
    /// <summary>
    /// 维修人员更新工单模型<br />
    /// 不同工单类型有相应子类
    /// </summary>
    public abstract class WorkOrderUpdateBaseDto : EntityDto<long>
    {
        /// <summary>
        /// 所属分类id
        /// </summary>
        public long? CategoryId { get; set; }
        /// <summary>
        /// 紧急程度
        /// </summary>
        public UrgencyDegree? UrgencyDegree { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Required]
        [StringLength(CoreConsts.OrderTitleMaxLength)]
        public string Title { get; set; }
        /// <summary>
        /// 关联的图片，第一张图片将作为封面
        /// </summary>
        public List<AttachmentEditDto> Images { get; set; }
        /// <summary>
        /// 内容描述
        /// </summary>
        [StringLength(CoreConsts.OrderDescriptionMaxLength)]
        public string Description { get; set; }
        ///// <summary>
        ///// 当前状态情况说明
        ///// </summary>
        //[StringLength(CoreConsts.OrderStatusChangedDescriptionMaxLength)]
        //public string StatusChangedDescription { get; set; }
        /// <summary>
        /// 预计开始时间
        /// </summary>
        public DateTimeOffset? EstimatedExecutionTime { get; set; }
        /// <summary>
        /// 预计结束时间
        /// </summary>
        public DateTimeOffset? EstimatedCompletionTime { get; set; }
        ///// <summary>
        ///// 员工id
        ///// </summary>
        //public string EmployeeId { get; set; }
    }

    /// <summary>
    /// 普通工单更新模型
    /// </summary>
    public class WorkOrderUpdateInput : WorkOrderUpdateBaseDto
    {
        ///// <summary>
        ///// 实体Id
        ///// </summary>
        //public string EntityId { get; set; }
        /// <summary>
        /// 扩展字段
        /// </summary>
        public IDictionary<string, object> ExtensionData { get; set; }
        /// <summary>
        /// 预留字段1
        /// </summary>
        public string ExtendedField1 { get; set; }
        /// <summary>
        /// 预留字段2
        /// </summary>
        public string ExtendedField2 { get; set; }
        /// <summary>
        /// 预留字段3
        /// </summary>
        public string ExtendedField3 { get; set; }
        /// <summary>
        /// 预留字段4
        /// </summary>
        public string ExtendedField4 { get; set; }
        /// <summary>
        /// 预留字段5
        /// </summary>
        public string ExtendedField5 { get; set; }
    }


}
