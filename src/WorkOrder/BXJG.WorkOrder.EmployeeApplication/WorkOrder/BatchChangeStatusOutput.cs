﻿using BXJG.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.EmployeeApplication.WorkOrder
{
    /// <summary>
    /// 员工批量改变工单状态的输出模型
    /// </summary>
    public class WorkOrderBatchChangeStatusOutput1Base : BatchOperationOutputLong
    {
    }
}
