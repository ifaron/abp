﻿using BXJG.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WorkOrder.WorkOrder
{
    /// <summary>
    /// 后台管理批量调整工单状态的输出模型基类
    /// </summary>
    public class WorkOrderBatchChangeStatusOutputBase : BatchOperationOutputLong
    {
    }
}
